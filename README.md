Secure Tech is a full service low-voltage contractor servicing Santa Clarita. We are licensed, bonded and insured. We specialize in Burglar and Fire Alarms, Security Cameras, Intercoms, Nurse Call Systems, Access Control, Custom Wiring and a full range of installation and maintenance services.

Address: 25132 Running Horse Rd, Santa Clarita, CA 91321, USA

Phone: 800-773-2873
